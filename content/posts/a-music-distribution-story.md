---
title: "A Music Distribution Story"
date: 2022-01-17T23:10:19+01:00
showDate: true
tags: ["music","story","producer","distribution"]
---

Hi Mates! 

So, in this post i would like to share my first experience in publishing my first new music album,
entirely for free money.

Now is a jungle out there: we have a lot of distributors, diritti d'autore royalties, music streaming services..
The real point is the marketing: everyone can publish easy, but only diamonds can shine (thanks to the best
marketing strategy).

# 1 New-Born: an entire produced OP-Z Album

![](/uploads/op-z.jpg)

This is my first publication: I am an introverse guy and probably I do not like to publish any kind of music I produce (fear? shy? Dunno...) but I decided to make this step. I bought my OP-Z in 2019 and I travelled a bit with it in my bag: it is small as a TV Remote. The instrument itself is really powerful because of the multiple things it can offer but probably when it comes to play the Synth Engines and make some Sound design... mmm it can be better (I hope for next releases).

So the album is a collection of sketches done in two years, and I think is it quite good in my opinion. You could say "..is yours!!! Why should you say the contrary?". I listened a lot of pieces on Youtube, FB Groups, Reddit Groups and I think the level sometimes is quite poor. But who am I to judge?

Anyway, pieces were organized in the Operator and then directly recorded into my iPad through AUM (google it) recording track by track as live set (more or less). I am not so practical to use DAW faster, so I decided to go through this way and actually I think it was a smart move for myself to be not locked in a DAW labirinth and let my creativity die(together with my release). 

The recording are taken with OP-Z as an Audio Interface, connected to my iPad Pro 2020 through a USB-C <--> USB-C connection: as an Audio Interface it can input and output Audio and it is more easy to record stuff (16 bit 44.1 Khz). The quality is good(not the best but good, especially with the latest release of the firmware I had less problem than I thought).

# 2 Mastering: Bandlab Mastering

![](/uploads/BandLabMastering.jpg)

From the subsection title you can understand which service I used to let the tracks to be mastered. The service is completely free and comes with 4 presets without giving any other settings to change parameters: you listen to the preset and you choose the most ideal one(based on your preferences) and... done! The track is ready to download and you can upload in your favourite distribution service or give your final product to some lables: if you are lucky they will distribute for you. 

Let's go little deeper with presets:

![](/uploads/BandLabMasteringpresets.jpg)
- <strong>Universal</strong>: Sound your best through any speaker. Organic dynamic processing and tonal enhancement.
- <strong>Fire</strong>: Stand out from the crowd. Pushed and punchy lows without muted vocals.
- <strong>Clarity</strong>: Let your idea be heard. Pristine breathy highs with light dynamic expansion.
- <strong>Tape</strong>: Aged to perfection. Warm tape saturation with analog dynamic processing.

Simple, free, effective. Of course, Pro services or Pro Audio engineers will bring better results (whatever the price is).

# 3 Distribution: Promote yourself(really easy in 2022)!
![](/uploads/musicphone.jpg)
I make it clear: Outhere is a Jungle (in Music World). It is really easy to find a free or paid distributor for your work. It is very hard to emerge in internet/real world. That is completely another job and won't be discussed in this post.

It is really easy to find different Music Distributor around the network and they propose three different ways to do this:

- Free with 80/20 Revenue share
- Monthly/Year paid Subscription
- One-Time pay (different prices based on singles, EP or LP)

I first give a list of available distributors specifying which is free or not:

- [Distrokid](https://distrokid.com/): Year subscription
- [CDbaby](https://cdbaby.com/cd-baby-cost/): One time fee (Divided in Standard and Pro releases)
- [TuneCore](https://www.tunecore.com/pricing): Year Subscription
- [Ditto](https://dittomusic.com/en/pricing/): Year Subscription
- [iMusician](https://imusician.pro/en/products/digital-music-distribution-pricing): One time fee per release
- [Landr](https://www.landr.com/it/tariffe/pubblicazioni): Mix with One time fee and year Subscription
- [RouteNote](https://routenote.com/pricing): Free or Year subscription
- [SoundDrop](https://soundrop.com/pricing/): 99 cents per track and 85/15 Revenue system
- [Amuse](https://www.amuse.io/en/pricing): Free or year subscription
- <s> DogmaDistro: Free with 80/20 revenue system </s>  ---> CLOSED.

If you want to take a look to the pricing just click the links above. Distributors have different prices and services, also they have different platforms where they distribute (Spotify, Tidal, Deezer...)

I intentionally excluded from the list [BandCamp](https://bandcamp.com/): if is your first time distribution and you are not fully comfort to distribute over the common platforms, Bandcamp is a good choice: First of all, is built for independent artists that want fully distribute by theirselves. It is free or with a monthly subscription (just for additional services) and you can put your releases for Free or with a price, including bonus tracks available only if people buy your album. If is the first time and you don't feel confident with the Modern Music Industry, Bandcamp is where a not known artist has to start in my opinion. 

I show how my album was published:

![](/uploads/bandcamp-logo.png)

1) First I released my LP on Bandcamp because I did not have the purpose to publish it on Spotify, Tidal or similar. I recorded 16 tracks, but I released 14 tracks to listen on bandcamp platform (streaming is limited to a certain number of listen) and 2 as bonus tracks because they're like a kind of demo. Anyway, Bandcamp try to pushes you to fix a price for the tracks and for the complete album, of course they earn from this because the revenue system is 75/25. 25% withholdings is not a small number but in the otherside, the 75% is directly sent to your Paypal account. I think this is the only platform that offers this service. Forget revenues from distributors if you are a small fish in the ocean: Music industry is now based on streaming services and before you start earning something, just relax and take it easy. In my opinion, if you are publishing album to make, or hope to make some cash with it, stop making music 'cause it will be frustrating. 


2) After some people took a listen to it, they liked my work and then someone suggested me to publish album through the streaming platforms. I decided to use a small Italian distributor: DogmaDistro from the Metatron label. Actually it has a lot of platform included and service is free (80/20 revenue system). The uploading system is careless if you look at the interface but is really easy and practical to use. In few words:

- upload the tracks
- organize tracks under an album with your specific order
- choose an image for your album
- choose all or just a bunch of streaming services you want to distribute
- give all the needed information about release date, collabs, label (if exist)...
- done.

Wait for release date and do some social media activity.

Just a tip: Spotify, Apple Music and others have the specific application for artists. Distributors won't do anything about that and you have to manage them. Also, PAY ATTENTION if you have the same name of other artists: unfortunately if you are a nobody and platforms doesn't know who you are, distributors can do some mistakes adding new artists. It is your job to have a look if everything is ok; if not just contact your distributor and make that fix it.

And that's my story! I tried to understand some mechanisms that were not known in my mind. In the end, besides making music, it was very instructive to go through all these processes. I am not so social and this is not a good point fo me, but I advise to spend sometime on Social platforms and increase (without any tricks) your followers in order to be more desirable to labels that distribute your genre of music.

See you at next story!

<center>{{< spotify type="album" id="0ExXVT992duBOPdIY6PwBH" width="50%" height="250" >}} </center>

EDIT 07/06/2022: Sadly, DogmaDistro closed their activities. I need to find a new distributor with moving all the tracks. I'll do an update of the new service I will choose. Also I want to try some decentralized blockchain based music streaming services like Audius or similar! 

EDIT2 12/10/2022: In the end I've chose Distrokid, I think the most famous service distributor for Indipendent Musicians (along with Bandcamp). Main things:
![](/uploads/distrokid.png)

- Cheap: yup, it is cheaper than other distributors but, some functionalities are not included, if you don't pay more than the annual fees. 
- Easy to use: you upload, leave descriptions and... done. You only need to wait that your songs appears on Music streaming services.
- ..and a lot of funny integrated services: Playlist wheels, AI imagery for your album artwork, Video Memes... Funny and free. 

I think this is very nice service if you want to start distribute. If you have any doubts there is a good and detailed FAQ Page:  [DistrokidFAQ-Support](https://support.distrokid.com/hc/en-us).

If you read this article and you liked it, please consider to support my work buying just a single song or the entire album on my bandcamp page (just follow the Music Prod. in homepage). 

In the end I want to leave here the video of my first live set ever as DPLZ with the songs from my album. Enjoy!!

{{< youtube OR8phehybNU >}}
