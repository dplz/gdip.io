+++
date = 2021-04-28T11:30:00Z
showDate = true
tags = ["D6", "GadosoRevolution2", "3dPrinter", "WanhaoGR2"]
title = "3D Printing Series: Wanhao GR2"

+++
# Wanhao Gadoso Revolution 2

Hello everybody! In this post, I will discuss about this 3D Printer by Wanhao: The GR2, an upgrade from D6/D6 plus.

![](/uploads/wanhaogr2.jpeg)

I will publish on this same page news about firmwares, mods and so on...

***

## My Honest (and Fast) Review

If you know the Duplicator 6 / 6 Plus you already know this printer: they share the same Kinematics and same structure. You can see it as a clone of Zortrax M200.

#### Downgrades

* **_Z-axis_**: D6 and Plus version had a **ball screw** for the build plate but GR2 has a simple T8 nut that could means less stability, but the axis is pushed inside directly in the stepper motor with an **integrated ball bearings**.

  ![](https://ae01.alicdn.com/kf/Hf639f00726ef42479f4c36bc830d14e4M.jpg)

  In my opinion, unless you have a bent Z-axis(which means a lot of vibration during print operation), it works very well and with a good resolution.
* **_Inductive Probe Sensor_**: D6 Plus has an inductive probe sensor that is missing on GR2. Actually is not a big problem because the calibration will remains after a lot of prints. Just be patient and do manual bed leveling. There is an option for Z-offset to change when you swap different print bed.

#### Upgrades

* **_32Bit Motherboard (STM32F103VE-T6)_**: the only bad thing here is that is **proprietary** MB.
* **_TMC2209 Drivers_**: compared to D6 and D6 plus this printer will be so much quieter. Sadly they are soldered.
* **_Touch Screen System_**: I put as an Upgrade, but probably it's the weak point of this printer. The screen is not classic tft screen managed by Marlin Firmware: behind the screen you can find an embedded board based on an old Allwinner A33 SoC. The screen has a small number of options, and during the print operation you can't touch almost any parameter except for temperature and print speed. The OS received two useless updates since I own the printer and I think that Wanhao stopped development. You "can" control the printer remotely... when cloud application works. If you use the Android application forget using it because never worked. If you own an iOS device probably it works when you are lucky. My advice is to replace with an Octoprint system with a Raspberry Pi. I will publish the mod in a future.

  ![](https://ae01.alicdn.com/kf/H3d8fb0e4343f4c5090497ed85646faf7V.jpg)

#### Extruder

I want to mention this, just because at the beginning, Wanhao sponsored the printer with a new **double gear extruder feeder.** But after some revisions they went back to the older system (see at Wanhao D6). So if you are planning to buy a new GR2, they ship with the older one.

#### Video Demo

{{< youtube D6wDY4cwUTQ >}}

**Sadly, Wanhao confirmed to me that the printer has been discontinued. Keep this page for Firmware updates, if there will be.**

***

## Firmware

Here we are!! I passed a lot of time chatting with Managers. After 6-7 Months, finally they gave me something to work with. After some testing now we are able to finally flash the new Marlin 2.0.8.2 update on the GR2. Everything seems to work ~~(except for Print Bed Calibration option that you must correct through any program that can work with GCode with M851 Z command)~~ and you can also set, for example, the stepper driver current, set microsteps and so on, directly from your favourite app that sends GCode commands (see the Marlin Documentation for more info).

Below you find the steps to flash the firmware. **Use this at your own risk. I am not responsible if you brick your GR2 Motherboard!**

* Install [CH340 drivers](https://gitlab.com/dplz/gdip.io/-/raw/master/content/posts/3D%20Printing/files/CH34x_Install_Windows_v3_4.zip "CH340 Driver") for Serial Communication (if you already have you can skip this step):

  {{< youtube X_xwFG794HM >}}
* Download the [Wanhao STM Update Tool](https://gitlab.com/dplz/gdip.io/-/raw/master/content/posts/3D%20Printing/files/3DSTUpFirmwareTool.zip "Wanhao STM Update Tool") . This application now works only on Windows and haven't found a way (yet) to flash the firmware with other applications. Anyway you can try to emulate if you are on Linux or Mac and see if it works. The usage is very simple:

  ![](/uploads/wanhaosttool.jpeg)
  1. Select the COM port in 'Serial number' (GR2 connected through the USB to PC)
  2. Select the .bin file (firmware below).
  3. Push Download: when the bar will reach 100% you're done!

  Now you can reboot the printer and you have your new Marlin 2.0!

  Once you can play the machine through GCode commands, please put in this way:

       M502
       M500

I will leave here all the bin file, also the oldest. The 2.0 is personally compiled by me and not by Wanhao.

List(from oldest to newer):

1. [Marlin 1.1.8 11/19/2019](https://gitlab.com/dplz/gdip.io/-/raw/master/content/posts/3D%20Printing/files/Marlin1-1-8_20191119.zip) - First release
2. [Marlin 1.1.8 01/05/2021](https://gitlab.com/dplz/gdip.io/-/raw/master/content/posts/3D%20Printing/files/Marlin1-1-8_20210105.zip) - Second Release (Unknown Changelog)
3. [Marlin 2.0.8.2 06/01/2021](https://gitlab.com/dplz/gdip.io/-/raw/master/content/posts/3D%20Printing/files/Marlin2-0-8-2_WanhaoGR2_060121_.bin "Marlin 2.0.8.2 GR2") - Newest release - Changelog:

* Based on latest stable Marlin release.
* Now you can set stepper current, microsteps, TMC enabled...
* Linear Advance active: value is set on 0. Try your own if you like but before you need to change TMC Drivers to Spreadcycle with **M569 S0 X Y Z** (E is already in Spreadcycle).
* ~~Nozzle Park Feature active~~ : I had to disable this because the original screen doesn't care at all.
* PrintBedCalibration working now from the original screen!
* **E TMC Driver** is now on spreadcycle mode
* It has been tested that now there aren't any lockdown when mounting new stepper motors with **0.9 degrees** or a **BMG Extruder**. You can do it and you don't need to change firmware settings but only some parameters with the GCode through Pronterface or whatever. For the BMG , it depends on which motor you mount but you only need to swap some cables. So that's it. For using 0.9 Stepper motors on X and Y you have to set **320 steps for X and Y**. _For BMG **you need 830 esteps in E for 1.8 motor or 1660 for 0.9 motor**_.The printer has a setting of 32 Microsteps, so don't worry for these higher values than normal.
* Fully compatible with Octoprint.
* To compile your own firmware just check my repo(I suggest VS Code):     [https://github.com/GiDiPa/Marlin-2.0.8.2-WanhaoGR2](https://github.com/GiDiPa/Marlin-2.0.8.2-WanhaoGR2 "https://github.com/GiDiPa/Marlin-2.0.8.2-WanhaoGR2")

\*After the Update you should probably redo your Z-Offset because the default offset is 0.

\** For Any operation to set new E-steps, Driver Current and so on... My advise is to use [Pronterface](https://github.com/kliment/Printrun/releases/tag/printrun-2.0.0rc8) ! For any of these operations, please check the [Marlin Documentation](https://marlinfw.org/meta/gcode/)!

**_I want to thank my "GR2 Team": @CosminMake, @Droylam, @luckylup. Many thanks for your support and contribute to this project!_**

***

## Mods

These mods I'm going to put here are personally chosen because I think they are the best Mods you can find for GR2 Printer. I won't show how to mount, and other things because it is really easy to understand: in any case you can ask on FB or Telegram Group!

**Use these at your own risk. I am not responsible if you damage some parts of your GR2 Printer!**

### Fun Ducts:

* Z-Storm: [https://www.thingiverse.com/thing:4906151](https://www.thingiverse.com/thing:4906151 "https://www.thingiverse.com/thing:4906151")
* Z-Vent: Just the heatsink slot or also the funduct if you want to install just a 4010 fan [https://www.thingiverse.com/thing:2797152](https://www.thingiverse.com/thing:2797152 "https://www.thingiverse.com/thing:2797152") (on telegram group you can find also a D-Storm that is almost the same thing, probably better adapted for Wanhao D6/GR2)
* 5015 Fan Duct System with Probe 8 Mount : [https://www.thingiverse.com/thing:4769322](https://www.thingiverse.com/thing:4769322 "https://www.thingiverse.com/thing:4769322") - if you don't want the probe mount but only the fun duct system

\*[Fan5015](https://www.amazon.it/gp/product/B08CCLZG5Z/ref=as_li_tl?ie=UTF8&camp=3414&creative=21718&creativeASIN=B08CCLZG5Z&linkCode=as2&tag=gdip-21&linkId=cde4146e51d4faddc432c09749a82a69) or [Radial Fan4010](https://www.amazon.it/gp/product/B07VHQZ5BT/ref=as_li_tl?ie=UTF8&camp=3414&creative=21718&creativeASIN=B07VHQZ5BT&linkCode=as2&tag=gdip-21&linkId=ea6b79b5495cd3001288bb28a44868ad) to buy!

### Replacing Display system (the crap of this printer):

Raspberry Mount and osoyoo DSI screen : [https://www.thingiverse.com/thing:4888731](https://www.thingiverse.com/thing:4888731 "https://www.thingiverse.com/thing:4888731") . You will find all the stuff to buy inside the link. We recommend them because we are working with that Items.

[Osoyoo Screen](https://amzn.to/3jeSwcw)

### BMG Extruder:

* [Original Bondtech](https://www.amazon.it/gp/product/B091TQJG4S/ref=as_li_tl?ie=UTF8&camp=3414&creative=21718&creativeASIN=B091TQJG4S&linkCode=as2&tag=gdip-21&linkId=97df45dc8a3f2f35a8d3f1e09687e63b)
* [Redrex Clone (which is quite good to me)](https://www.amazon.it/gp/product/B07Q5RNRR6/ref=as_li_tl?ie=UTF8&camp=3414&creative=21718&creativeASIN=B07Q5RNRR6&linkCode=as2&tag=gdip-21&linkId=557f6077f9e97e1b713aa7675b9f81ba)
* [Trianglelab BMG V2](https://s.click.aliexpress.com/e/_9HrzGQ) - So gooood but you need to wait a bit.

### BMG Support:

* [BMG support by liftbag](https://gitlab.com/dplz/gdip.io/-/raw/master/content/posts/3D%20Printing/files/Liftbag_BMG_bracket_Base_.zip) (my first choice for Flat cable protection) - [Liftbag Model Page](https://www.thingiverse.com/liftbag/designs)
* BMG with inclined flat cable support - [https://www.thingiverse.com/thing:4642479](https://www.thingiverse.com/thing:4642479 "https://www.thingiverse.com/thing:4642479")
* BMG optimized for Trianglelab BMG 2.0 - [https://www.thingiverse.com/thing:3388721](https://www.thingiverse.com/thing:3388721 "https://www.thingiverse.com/thing:3388721")
* BMG for original motor - [https://www.thingiverse.com/thing:3779339](https://www.thingiverse.com/thing:3779339 "https://www.thingiverse.com/thing:3779339") (the original motor doesn't fit in Y size, so if you prefer you swap with a pancake motor - **see Motor mod** - or you can stay with this).

### OMG Extruder and support:

If you don't want to lose any mm in Y(with BMG happens if you stay with the original motor) this could be a good solution for you!

* OMG Extruder - [Here!](https://s.click.aliexpress.com/e/_A9KhIW)
* OMG Support - [https://www.thingiverse.com/thing:4939443](https://www.thingiverse.com/thing:4939443 "https://www.thingiverse.com/thing:4939443")

### Motor Mod:

There's not too much to write here: the best thing you can do is swap the 1.8 Moons motor(believe me, except for Extruder motor that is too low in Ampere, the other are good). But if you want improve precision, more stability and engine torque then you need 0.9 motor. Actually I don't know what to advise for Z but, for X, Y and E you can use [https://e3d-online.com/products/motors](https://e3d-online.com/products/motors "https://e3d-online.com/products/motors") (2 Compact but powerful and 1 Slimline motor).

### Filament Sensor:

\*At this state there is no hope to make a filament sensor work with GR2 Stm32f103 Mainboard. So in any case you have to use a raspberry pi installed to use this, with plugins you can find on Octoprint Plugin Manager. For connection see the Raspberry Pi Tutorial below.

* Re-Use your Z-max endstop - [https://www.thingiverse.com/thing:2274534](https://www.thingiverse.com/thing:2274534 "https://www.thingiverse.com/thing:2274534")
* Smart Filament Sensor BTT - [download](https://gitlab.com/dplz/gdip.io/-/raw/master/content/posts/3D%20Printing/files/extended.stl?inline=false) by [CosminMake](https://www.thingiverse.com/cosminmake/designs)

BTT Smart Filament sensor - [here!](https://amzn.to/3knWkaq)

### Inductive Probe Sensor for Autoleveling:

Sadly, is not possible to mount a BL-Touch. We tried but is an hard work and probably is better to buy a new SKR or MKS board to do this. It's up to you. To connect this on motherboard follow the Raspberry Pi Tutorial below.

\*It is not possible to use the inductive probe sensor with Z-Storm and Z-Vent because the optimal place we found is only on the front and with that stuff is almost impossible. Try your own solution if you don't like. We strongly suggest to use the 12mm inductive probe because 8mm could be very unstable if you swap the build plate.

* Inductive Probe Sensor 12mm :  Buy [here ](https://www.amazon.it/gp/product/B07XMND4QN/ref=as_li_tl?ie=UTF8&camp=3414&creative=21718&creativeASIN=B07XMND4QN&linkCode=as2&tag=gdip-21&linkId=87525389382381967f80e649f9a8a627)or find in your amazon country(you also need to crimp the pin so I suggest [this Kit](https://www.amazon.it/gp/product/B07QV4GKF8/ref=as_li_tl?ie=UTF8&camp=3414&creative=21718&creativeASIN=B07QV4GKF8&linkCode=as2&tag=gdip-21&linkId=451a539c791dd636224e6115af8cd3f2)).
* Mount for Masp V1: [https://www.thingiverse.com/thing:4937390](https://www.thingiverse.com/thing:4937390 "https://www.thingiverse.com/thing:4937390")
* Simple mount :  [https://www.thingiverse.com/thing:4890415](https://www.thingiverse.com/thing:4890415 "https://www.thingiverse.com/thing:4890415")

### Hotend:

You don't have to many options: or you stick with MK10 or you can use some V6 (or also Volcano) hotend adapted for GR2.

**For MK10:**

* Cheap solution: [Trianglelab Micro Swiss MK10 hotend ](https://s.click.aliexpress.com/e/_AL7oty)
* Original: [MicroSwiss Mk10 All metal hotend](https://www.amazon.it/gp/product/B01C3HEQZC/ref=as_li_tl?ie=UTF8&camp=3414&creative=21718&creativeASIN=B01C3HEQZC&linkCode=as2&tag=gdip-21&linkId=72313d0dae56263cc9077076008eaf87)
* [Original with Better Heatsink](https://www.amazon.it/gp/product/B01JPHLSD6/ref=as_li_tl?ie=UTF8&camp=3414&creative=21718&creativeASIN=B01JPHLSD6&linkCode=as2&tag=gdip-21&linkId=923d522b0c047d34df583e071a5fa60b)

**V6 solutions:**

* MASP Solutions : [V1](https://www.masp-solutions.it/product-page/masp-hotend-wanhao) or[ V2](https://www.masp-solutions.it/product-page/masp-hotend-mk10). [Contact him](https://www.facebook.com/solutions.masp) to ask which is the better solution for you if you interested.
* [Copper Heatbreak Mk10 to v6](https://s.click.aliexpress.com/e/_ApxeaW) + [Lerdge Block](https://s.click.aliexpress.com/e/_Ab0eya) (choose the one you prefer but you need to change also [Thermistore ](https://s.click.aliexpress.com/e/_AcILJU)and [Heater](https://s.click.aliexpress.com/e/_A7TnF4)) + A V6 Nozzle.

In both cases I suggest to buy a [Nozzle X from V6](https://www.amazon.it/gp/product/B07WTX3CP6/ref=as_li_tl?ie=UTF8&camp=3414&creative=21718&creativeASIN=B07WTX3CP6&linkCode=as2&tag=gdip-21&linkId=ab1832d9aa7c1d9faccfd63c4856b3bd).

### Pi Cam:

If you are going to do the Raspberry Pi Mod, it is recommended to buy a Pi Cam because the original webcam won't work with a usb cable (a lot of green noise).

Pi Cam Support - coming soon

Pi Cam to buy - [Here]()

#### Bonus! Magnetic Build plate:

It's a cost, but it's very confortable and easy to use for removing printed object from the bed. Pei double side (smooth and textured) 220x220 with a magnet plate to apply!

[Buy here!](https://www.amazon.it/gp/product/B095Z3GJR8/ref=as_li_tl?ie=UTF8&camp=3414&creative=21718&creativeASIN=B095Z3GJR8&linkCode=as2&tag=gdip-21&linkId=9e5edb655677e362a96f3b34c9fd2c08)

Or (if you have patience):

[Buy here!](https://s.click.aliexpress.com/e/_9f0Gno)

#### Bonus pt.2:

If you have enough space for your upper plexiglass and you want cables to have better movements (also for filaments) print this:

[https://www.thingiverse.com/thing:4937430](https://www.thingiverse.com/thing:4937430 "https://www.thingiverse.com/thing:4937430")

#### Bonus pt.3:

PTFE Tube. Buy a better PTFE Tube for your filament!

* [Capricorn here](https://amzn.to/3koQ16q)
* [TriangleLab here](https://s.click.aliexpress.com/e/_A1luMW)

## Raspberry Pi + Octoprint + Octodash Tutorial:

If you setup the printer as I show in the video down below, please consider to flash this firmware - [Marlin 2.0.8.2 Mod Probe](https://gitlab.com/dplz/gdip.io/-/raw/master/content/posts/3D%20Printing/files/Marlin2-0-8-2_GR2_Mod_Probe_.bin?inline=false)

Octoprint Image to flash with [balenaEtcher](https://www.balena.io/etcher/) + configuration files : [https://mega.nz/folder/QYp0yaTS#eTYCNf1P4HkXOM5y0TM4sA](https://mega.nz/folder/QYp0yaTS#eTYCNf1P4HkXOM5y0TM4sA "https://mega.nz/folder/QYp0yaTS#eTYCNf1P4HkXOM5y0TM4sA")

{{< youtube rpTCBtSNJRE >}}

Password for access Octoprint page:

* User : wanhaogr2
* Pass : wanhaogr2

Access in ssh(you must configure Octopi-WPA-Supplicant file to access internet):

    ssh pi@octopi.local
    
    #password is "raspberry"

Raspberry Pi GPIO Scheme Connection(PI 2, PI 3, PI 4):

![](/uploads/photo5848418003476133815.jpg)

Pin Scheme of BTT Filament Sensor:

![](/uploads/photo5848418003476133816.jpg)

**N.B.:** _If you want to use an Endstop as filament sensor, probably you should connect only GPIO 24 and Ground without 5V pin. I am not sure about this, but there is another plugin for this kind of filament sensor. If you're interested in this other kind, do your own research :) !_

***

## Groups:

**Telegram (Italian Group):** [https://t.me/wanhaogr2ita](t.me/wanhaogr2ita "Wanhao GR2 Ita Telegram")

**Facebook (English Group):** [https://www.facebook.com/groups/673187166794113](https://www.facebook.com/groups/673187166794113 "Wanhao GR2 Facebook Group")

***

### Donate

If you like my job, please feel free to donate me a coffee :) !

paypal email: giacomo.dipaolo@outlook.com

(Don't have a Paypal button yet XD )

#### Updates:

08-24-2021: on this date, I consider my work done on this printer. I'm sure that I won't publish anything for a long time. I will consider update Marlin firmware only if something cool new functions are going to be implemented. See ya!