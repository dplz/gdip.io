+++
date = 2022-12-11T15:00:00Z
showDate = true
tags = ["EndpointCentral", "SysAdmin", "ActiveDirectory", "ManageEngine"]
title = "Sketchbook: Manage Engine - Endpoint Central"

+++
These are just some Sketches about the work did in **Edotto SRL** about some **Manage Engine - Endpoint Central** functionalities.

### Manage Engine - Endpoint Central

What this application is exactly?

**Endpoint Central** (formerly Desktop Central) is a **unified endpoint management** and security solution that helps in managing servers, laptops, desktops, smartphones, and tablets from a central location. It's a modern take on desktop management that can be scaled as per organizational needs.

{{< youtube Pe85BH9SiS8 >}}

Down below will be shown the functionalities that I've learned during my job. They could be incomplete or perhaps not the best practice.

[![](/uploads/home.png)](/uploads/home.png)

Home: it is possible to see the dashboard just for having the actual situation of all pc, configurations and so on.

#### Patch Management

Patch management is useful to keep on all Security patches on all Domain's PCs. It is possible also to update third party software but I've found unstable, so I had used it only for critical and important updates of OS.

[![](/uploads/patchmm.png)](/uploads/patchmm.png)

It is possible to keep an eye also to all Missing or Installed patches on PCs you manage.

[![](/uploads/patchmm2.png)](/uploads/patchmm2.png)

##### Manual patch management:

Actually, it is possible to use Manual patching if only need to patch a single computer, otherwise Automatic is useful for all pc in the business network:

[![](/uploads/patchmm4.png)](/uploads/patchmm4.png)

You can see which manual configuration has been deployed in past operations and create a new one with "Install/Uninstall patch" and you'll see this screen:

[![](/uploads/patchmm5.png)](/uploads/patchmm5.png)

Select the patch you need to install to a specific single/group of PCs and then click on deploy!

##### Automatic Patch updates

Automatich Patch updates are useful to keep the workspace as safe as possible updating the 0day fixes and OS updates.

On the left click on automatic updates ( when you are on Patch Tab) and you will see this screen:

[![](/uploads/patchmm7.png)](/uploads/patchmm7.png)

You have an history of the Automatic patches applied, that keep on updating the automatic task you previously assigned. To create a new on click on "new task" and a new page is going to open:

[![](/uploads/patchmm8.png)](/uploads/patchmm8.png)

4 steps are required and they are on the left. The most important thing is on second page where you have to select the update type to the task. **I recommend going for Security Updates on Important and Critical, and Service Pack and Feature Pack for the OS.**

These updates are going automatically during the week, and they will do their job. If something is not working you can check the summary (as task images above) and see what kind of problem is happening.

#### Inventory

Inventory tab is a focus on every single device you added in UEM. You can check almost every aspect of a device and do some operations.

[![](/uploads/inventory.png)](/uploads/inventory.png)

On the left you can click on Computers and:

[![](/uploads/inventory2.png)](/uploads/inventory2.png)

Here there is a table with the PCs that have the agent installed. Red icon mean is disconnected, green means device is online.

_Tip: sometimes red and green are not real, probably a pc has been disconnected from few minutes and the UEM did not check the status. But if you click on the pc icon, it will instantly refresh (when red to green could take more seconds to change state)._

When icon is green you can see these functions:

[![](/uploads/inventory4.png)](/uploads/inventory4.png)

If you click on pc name, you will have a page with multiple tabs that has detailed information on that specific pc:

[![](/uploads/inventory3.png)](/uploads/inventory3.png)

In here it is possible to do many operations as described.

#### Tools

There is a Tools Page to do massive actions instead of using atomic operations in Inventory Pages. It is very easy to understand, looking to the left, which massive operation are available:

[![](/uploads/tools.png)](/uploads/tools.png)

#### Reports

This is a useful page if you need some kind of reports that other department need to trace inside the company. I did not used in my time, but is quite easy to have look and program scheduled reports:

[![](/uploads/reports.png)](/uploads/reports.png)

#### Agent

Agent is the main application that is installed in Business devices to let the UEM control them. Without the installation of agents, it is not possible to control any function or any deploy in Devices.

[![](/uploads/agent.png)](/uploads/agent.png)

[![](/uploads/agent2.png)](/uploads/agent2.png)

Of course agent could be installed in different Domains: it could be possible that if you have an installed Active Directory central server, you don't have possibility, after 60 days, to control a device inside the domain: this is an example of people always in Smart Working. This is very useful for the Agents to cooperate between different domains.

[![](/uploads/agent3.png)](/uploads/agent3.png)

The "Office" could be, for example, an example of all devices registered to a specific company, indipendently from Active Directory Domains (as explained above). That's the common way to go if you have heterogeneous situations.

[![](/uploads/agent4.png)](/uploads/agent4.png)

There is also a Table List where you can see where agents are installed.

If you want to install agent to a new device you can follow steps as the image down below shows:

[![](/uploads/agentinstallation.png)](/uploads/agentinstallation.png)

#### Configurations

Will be described ASAP.

[![](/uploads/configurations.png)](/uploads/configurations.png)

[![](/uploads/configurations2.png)](/uploads/configurations2.png)

[![](/uploads/configurations3.png)](/uploads/configurations3.png)

[![](/uploads/configurations4.png)](/uploads/configurations4.png)

[![](/uploads/configurations5.png)](/uploads/configurations5.png)

#### Software Deployment

Will be described ASAP.

[![](/uploads/swdeployment.png)](/uploads/swdeployment.png)

[![](/uploads/swdeployment2.png)](/uploads/swdeployment2.png)

[![](/uploads/swdeployment4.png)](/uploads/swdeployment4.png)

[![](/uploads/swdeployment5.png)](/uploads/swdeployment5.png)

[![](/uploads/swdeployment6.png)](/uploads/swdeployment6.png)

[![](/uploads/swdeployment7.png)](/uploads/swdeployment7.png)

[![](/uploads/swdeployment8.png)](/uploads/swdeployment8.png)