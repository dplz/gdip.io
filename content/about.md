---
title: 'It''s me: GDiP'
date: 2021-01-11T18:14:50.000+01:00

---
Hello World! I'm Giacomo. I am 30 and I live at Spoleto(PG). I am a Computer Scientist with a Master Degree and I work as **_Logistic consultant in PTV Group_**. My Main activities now are Software Development solutions, 3D Print Business and Music Event with my cultural association: **WoWo**.

I had different works experience in Music and Food Business but just for passion!

Whenever I can, I like to escape from my city because I really love to travel!!

Please, contact me here:

> * giacomo.dipaolo@outlook.com
> * [telegram](http://t.me/diploz): @DiPLoZ