+++
date = 2022-12-10T15:00:00Z
showDate = true
tags = ["Domain Controller", "MacOS", "ManageEngine", "AD"]
title = "Sketchbook:  Active Directory"

+++
These are just some Sketches about the work did in **Edotto SRL** about some **Active Directory** functionalities.

### Active Directory

![](/uploads/hero_activedirectory.jpg)

[**Active Directory**](https://en.wikipedia.org/wiki/Active_Directory "Active Directory") (**AD**) is a directory service developed by Microsoft for Windows domain networks. It is included in most Windows Server operating systems as a set of processes and services. Initially, Active Directory was used only for centralized domain management. However, Active Directory eventually became an umbrella title for a broad range of directory-based identity-related services.

I will skip the installation of Windows Server to configure AD, but there are many ways to install it (VMWare , Dedicated Server).

Managing People and Devices is quite simple. You can reach server with RemoteDesktop application:

![](/uploads/screenshot-2022-11-29-101326.png)

Once opened:

![](/uploads/screenshot-2022-11-29-101520.png)

As you can see in the above example, it is possible to see the domain forest with all the group folder you can manage as you want. In this case, We can see below the _domain controllers_ folder, it has been organized to have Computers, Users, MacOS and Groups:

* Users: it is possible to add, update, remove users and manage informations about passwords and so on. The user has to be a person inside the organization to manage the Company's PCs or MacOS Systems. It is possible to create some fake users to test some functionalities about AD. ![](/uploads/screenshot-2022-11-29-103401.png)

  This is the lists of users. If you click where showed in image above it is possible to add a new user:

  ![](/uploads/screenshot-2022-11-29-104141.png)

  Insert data when you are inserting a new user.

  It is possible to edit added users with right click:![](/uploads/screenshot-2022-11-29-105153.png)
* Groups: as the name suggest it is possible to create groups where users can be added. The main purpose is that we can assign policies to a group instead losing time to assign to all kind of users. This is useful and less time consuming.

  ![](/uploads/screenshot-2022-11-29-110036.png)
* Computers and MacOS: These are two folders with all pc and mac added to the domain.![](/uploads/screenshot-2022-11-29-112938.png)

  With Windows there are two ways to add pc in AD: the first is integrated in windows systems (only Pro versions can be added, Windows home edition will not be added) if we are adding brand new unused pc. Otherwise if you need to manage a pc with all data and programs already installed you can use this program:  [ForensIT](https://www.forensit.com/downloads.html "ForensIT") . It is a nice way to migrate a computer from domain to other domain keeping all the data. It is really easy to use

{{< youtube ugTbW6_Qakc >}}

**Active Directory For MacOS Systems:**

{{< youtube i7dwpX_D1Cg >}}

But remember to select Mobile Account otherwise, mac AD account won't work outside the AD Network.

The difference to have two kinds of device folders is necessary to assign different group policies because there is no compatibility between MacOS Systems and Windows devices.

The groups are useful, for example, to have a shared folder inside the AD network and give permissions to specific user(s) or specific group(s).

These are just some sketches from the things I've studied and learnt during my previous work in Edotto SRL. 

Hope someone can find useful things in here!

Cheers! :)