---
title: Works
url: "/works"

---
## Companies

Companies that I worked for or I am working with:<br><br>

![PTV Group](/uploads/ptvgroup1.jpg)

PTV Group as **Logistic Consultant and Developer** | January 2023 - OnGoing<br><br>
**Empowering logistics and mobility – for a cleaner and smarter future.**

![Edotto SRL](/uploads/edotto.png)

Edotto SRL as **Developer & IT Tech** | June 2022 - December 2022

Very nice experience during 2022 in IT Department at Edotto SRL as **Junior Developer and Sys Admin**.  

![Digipass](/uploads/logo-digipass-trasp-300x73.png)

Digipass Spoleto as **Digital Facilitator** | 2019  - 2022

The DigiPASS are public and open spaces with free access, where you can find an expert available to let citizens and businesses learn to use digital services.
<br>

***
## My Business

<br><br>
![WowoFactory](/uploads/wowo.jpg)

Wowo APS as **Cofounder** | 2022 - OnGoing

Together with **Matteo Mancini and Massimo Balducci**, we shared our passions into strong Association: Music, Video, Graphics and Communication. We focus on organize events on our Territory and Beyond. 

**[Wowo website](https://www.wo-wo.it)**

![Futuratech](futuratech-resized.png)

Futuratech srls as **Cofounder** | 2020 - Inactive

Together with **Federico Di Paolo**, we started a 3D Print business activity focusing on Medical devices and Prototyping environment for our clients. Nowadays, we are in a situation of R&D.

<br><br>

***

***

<br>

## Projects

[Github Repo](https://github.com/GiDiPa)

<br><br>

## Customers and Partners

{{% notableborder %}}
| ![](/uploads/alaracing-1.png) | ![](/uploads/trasco-1.png) | ![](/uploads/ecofutura.png) |
| --- | --- | --- |
| ![](/uploads/duranti.png) |  |  |
{{% /notableborder %}}
 