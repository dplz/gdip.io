---
title: "Photos"
url: "/photos"
---

{{< gallery match="images/*" sortOrder="desc" rowHeight="150" margins="5" resizeOptions="600x600 q90 Lanczos"  previewType="blur" embedPreview="true" >}}