---
title: "Welcome to my Brain"
date: 2021-01-20T16:37:34+01:00
showDate: true
tags: ["blog","story","begin","start"]
---

Ciao a tutti! Inserisco questo primo post breve per dire cosa andrò ad inserire nel blog come argomenti:

* *Tecnologia*
* *Stampa 3d (anche se sono ancora un noob)*
* *Informatica (non guide ma esperimenti personali)*
* *Musica*
* *Libri*

Non ci saranno post a cadenza regolare: non sono un vero blogger e non amo molto parlare dei fatti personali,
ma qualora ci fosse qualcosa che ritengo interessante, **ma soprattutto avrò la voglia e il tempo di scriverlo**,
allora vedrete qualche nuovo post(non sono in nessun social al momento...). 


See ya!

